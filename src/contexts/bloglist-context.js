import { createContext, useContext, useState } from "react";


const BlogListContext = createContext()

function BlogListProvider(props) {

    const [blogList, setBlogList] = useState([])

    return (
        <BlogListContext.Provider value={{ blogList, setBlogList }} {...props} />
    )
}

const useBlogList = () => useContext(BlogListContext)

export { BlogListProvider, useBlogList }