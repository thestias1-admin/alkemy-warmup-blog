import { Modal, Button, Form } from 'react-bootstrap';
import { useState } from 'react'
import UpdateBlogForm from '../UpdateBlogForm/UpdateBlogForm'
import axios from 'axios';
import { useBlogList } from '../../contexts/bloglist-context';

function BlogActions(props) {

    const { blogList, setBlogList } = useBlogList()
    const [show, setShow] = useState(false);

    // Managing Show/Close Modal
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function removeBlog(event) {
        let blogID_to_remove = Number(event.target.parentElement.parentElement.id)
        let newBlogList = (blogList.filter(blog => blog.id !== blogID_to_remove)) // Removing the blog from the list
        // Forcing a Re-Render when blog is filtered
        setBlogList(newBlogList)
        // Updates the cached BlogList in case the page is refreshed 
        sessionStorage.setItem('blogList', JSON.stringify(newBlogList))
        event.target.parentElement.parentElement.style.display = 'none' // Hidding the blog
        axios.delete('https://jsonplaceholder.typicode.com/posts/' + props.blogData.id)
    }

    return (
        <>
            <div className="d-flex gap-3">
                <Button variant="info" size="sm" onClick={handleShow}>Edit</Button>
                <Button variant="danger" size="sm" onClick={event => removeBlog(event)}>Remove</Button>
            </div>

            {/* Modal for editing the blog */}
            <Modal show={show} onHide={handleClose} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Edit Blog</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {/* Form for Updating the Blog! */}
                    <UpdateBlogForm blogData={props.blogData} />
                </Modal.Body>
            </Modal>
        </>
    )

}

export default BlogActions