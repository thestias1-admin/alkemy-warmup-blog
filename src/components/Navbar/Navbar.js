import { Nav, Navbar, Container, Form, Button, FormControl } from "react-bootstrap"
import { Link, useHistory } from "react-router-dom";
import { useAuth } from "../../contexts/auth-context";
import './Navbar.css'


function NavigationBar() {

    const { isLogged, setIsLogged } = useAuth()

    const history = useHistory()

    function Logout() {
        window.localStorage.removeItem('token')
        setIsLogged(false)
        history.push('/login')
    }

    return (
        <Navbar bg="dark" variant="dark" expand="lg" className="border-bottom border-light border-1 fw-bold mb-4">
            <Container>
                <Navbar.Brand className="text-warning">Alkemy Blog</Navbar.Brand>

                <Navbar.Toggle aria-controls="responsive-navbar-nav" />


                {isLogged ?
                    /* If the user is logged */
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="ms-auto">
                            <Nav.Link as={Link} to="/">Home</Nav.Link>
                            <Nav.Link as={Link} to="/new-blog">New Blog</Nav.Link>
                            <Nav.Link onClick={Logout}>Logout</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                    :
                    <Navbar.Collapse className="justify-content-end" id="responsive-navbar-nav">
                        <Nav.Link as={Link} to="/">Home</Nav.Link>
                        <Nav.Link as={Link} to="/login">Login</Nav.Link>
                    </Navbar.Collapse>
                }
            </Container >
        </Navbar >
    )
}


export default NavigationBar