import { useFormik } from 'formik';
import * as yup from 'yup';
import { Form, Button } from 'react-bootstrap';
import { useState } from 'react';
import { Loading, isLoading } from '../LoadingComponent/LoadingComponent'
import axios from 'axios';
import { useBlogList } from '../../contexts/bloglist-context';



function NewBlogForm() {
    // This function creates a blog given a props with a Object blogData and whitin <id>, <title> and <body>

    // Blog List context
    let { blogList, setBlogList } = useBlogList()

    const [loading, setLoading] = useState(false)
    const [createBlogErrors, setCreateBlogErrors] = useState(null)
    const [successCreated, setSuccessCreated] = useState(false)

    // Setting up initial values for formik
    const initialValues = {
        title: '',
        body: ''
    }

    // Validating Form Fields
    const validationSchema = yup.object({
        title: yup.string().required('This field is required'),
        body: yup.string().required('This field is required')
    })

    const onSubmit = values => {
        axios.post('https://jsonplaceholder.typicode.com/posts', {
            title: values.title,
            body: values.body,
            userId: Number(window.localStorage.getItem('userID'))
        })
            .then(res => {
                if (res.status === 201) {
                    setLoading(false)
                    setSuccessCreated(true)
                    // Getting the cached blog list, adding the new element and setting the context with the new data
                    let cachedBlogList = JSON.parse(sessionStorage.getItem('blogList'))
                    cachedBlogList.push(
                        {
                            id: cachedBlogList.length + 1,
                            title: values.title,
                            body: values.body,
                            userId: Number(localStorage.getItem('userID'))
                        }
                    )
                    setBlogList(cachedBlogList)
                    // Updating the cache
                    sessionStorage.setItem('blogList', JSON.stringify(cachedBlogList))
                }
            })
            .then(err => {
                console.log(err)
                setCreateBlogErrors(err)
            })
    }

    const formik = useFormik({
        initialValues,
        onSubmit,
        validationSchema
    })

    // End Formik

    return (
        <Form method="POST" onSubmit={formik.handleSubmit} className="text-light">

            <Form.Group controlId="formTitle" className="my-3">
                <Form.Label>Title</Form.Label>
                <Form.Control name="title" type="text" placeholder="Edit Title" {...formik.getFieldProps('title')} />
                {/* Error Handling */}
                {formik.touched.title && formik.errors.title ? <p className="mt-1 text-danger">{formik.errors.title}</p> : null}
            </Form.Group>

            <Form.Group controlId="formBody">
                <Form.Label>Body</Form.Label>
                <Form.Control name="body" as="textarea" placeholder="Edit Body" style={{ height: '300px' }} {...formik.getFieldProps('body')} />
                {/* Error Handling */}
                {formik.touched.body && formik.errors.body ? <p className="m-0 mt-1 text-danger">{formik.errors.body}</p> : null}
            </Form.Group>

            <Button className="mt-4" variant="warning" type="submit" onClick={() => setLoading(true)}>{isLoading(loading) ? <Loading /> : 'Enviar'}</Button>
            {createBlogErrors ? <p className="fw-bold mt-4 text-danger">{createBlogErrors}</p> : null}
            {successCreated ? <p className="fw-bold mt-4 text-success">Successfully created blog!</p> : null}
        </Form>
    )
}

export default NewBlogForm