import { Container } from 'react-bootstrap'
import { useParams } from 'react-router-dom';

function BlogView() {

    let { blogID } = useParams()
    let blogList = JSON.parse(sessionStorage.getItem('blogList'))
    let blogData = { title: null, body: null }


    const findBlog = () => {
        // Finds the blog!
        blogList.forEach(element => {
            if (element.id == Number(blogID)) {
                blogData = element
                return
            }
        });
    }

    return (
        <Container className="text-light">
            {findBlog()}
            <h2 className="border-bottom">{blogData.title}</h2>
            <p>
                {blogData.body}
            </p>
        </Container>
    )
}

export default BlogView