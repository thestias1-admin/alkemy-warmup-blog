import { ListGroup } from 'react-bootstrap'
import BlogActions from '../BlogActions/BlogActions'
import { Link } from 'react-router-dom'

function BlogLink(props) {
    // Renders the Blogs links on the Homepage
    let index = props.index
    let blog = props.blog
    let userID = window.localStorage.getItem('userID')


    function truncate_body(string, limit) {
        // Creates a truncated string based on a <limit>
        if (string.lenght <= limit) { return string }
        return string.slice(0, limit) + '...'
    }

    function is_blog_owner() {
        if (userID == props.blog.userId) {
            return (<BlogActions blogData={props.blog} />)
        }
    }

    return (
        <ListGroup.Item key={index} id={blog.id} className="text-light bg-dark border-light">
            <h4>
                <Link to={'/blog/' + blog.id} className="anchor-tag">{blog.title}</Link>
            </h4>
            <p>{truncate_body(blog.body, 50)}</p>
            {is_blog_owner()}
        </ListGroup.Item>
    )
}

export default BlogLink