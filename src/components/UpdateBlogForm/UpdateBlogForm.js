import { useFormik } from 'formik';
import * as yup from 'yup';
import { Form, Button } from 'react-bootstrap';
import { useState } from 'react';
import { Loading, isLoading } from '../LoadingComponent/LoadingComponent'
import axios from 'axios';
import { useBlogList } from '../../contexts/bloglist-context';




function UpdateBlogForm(props) {
    // This function updates a blog given a props with a Object blogData and whitin <id>, <title> and <body>

    // Blog List Context
    let { blogList, setBlogList } = useBlogList()
    // Getting the Blog data!
    let blogId = props.blogData.id
    let blogTitle = props.blogData.title
    let blogBody = props.blogData.body

    const [loading, setLoading] = useState(false)
    const [updateBlogErrors, setUpdateBlogErrors] = useState(null)
    const [successEdited, setSuccessEdited] = useState(false)

    // Setting up initial values for formik
    const initialValues = {
        title: blogTitle,
        body: blogBody
    }

    // Validating Form Fields
    const validationSchema = yup.object({
        title: yup.string().required('This field is required'),
        body: yup.string().required('This field is required')
    })

    const onSubmit = values => {
        axios.put('https://jsonplaceholder.typicode.com/posts/' + blogId, {
            id: blogId,
            title: values.title,
            body: values.body,
            userId: Number(window.localStorage.getItem('userID'))
        })
            .then(res => {
                if (res.status === 200) {
                    setLoading(false)
                    setSuccessEdited(true)
                    let blogList = JSON.parse(sessionStorage.getItem('blogList'))
                    // Modifiying the corresponding blog
                    let newBlogList = blogList.map(el => el.id === blogId ? { ...el, body: values.body, title: values.title } : el);
                    setBlogList(newBlogList)
                    sessionStorage.setItem('blogList', JSON.stringify(newBlogList))
                }
            })
            .then(err => {
                console.log(err)
                setUpdateBlogErrors(err)
            })
    }

    const formik = useFormik({
        initialValues,
        onSubmit,
        validationSchema
    })

    // End Formik

    return (
        <Form method="POST" onSubmit={formik.handleSubmit}>

            <Form.Group controlId="formTitle" className="my-3">
                <Form.Label>Title</Form.Label>
                <Form.Control name="title" type="text" placeholder="Edit Title" {...formik.getFieldProps('title')} />
                {/* Error Handling */}
                {formik.touched.title && formik.errors.title ? <p className="mt-1 text-danger">{formik.errors.title}</p> : null}
            </Form.Group>

            <Form.Group controlId="formBody">
                <Form.Label>Body</Form.Label>
                <Form.Control name="body" as="textarea" placeholder="Edit Body" style={{ height: '300px' }} {...formik.getFieldProps('body')} />
                {/* Error Handling */}
                {formik.touched.body && formik.errors.body ? <p className="m-0 mt-1 text-danger">{formik.errors.body}</p> : null}
            </Form.Group>

            <Button className="mt-4" variant="warning" type="submit" onClick={() => setLoading(true)}>{isLoading(loading) ? <Loading /> : 'Enviar'}</Button>
            {updateBlogErrors ? <p className="fw-bold mt-4 text-danger">{updateBlogErrors}</p> : null}
            {successEdited ? <p className="fw-bold mt-4 text-success">Successfully edited blog!</p> : null}
        </Form>
    )
}

export default UpdateBlogForm