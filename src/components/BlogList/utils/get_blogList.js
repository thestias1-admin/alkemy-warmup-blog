import axios from "axios"

export async function getBlogList() {
    try {
        let blog_list = await axios.get('https://jsonplaceholder.typicode.com/posts')
        if (blog_list.status === 200 && blog_list.data) {
            return blog_list.data
        }
    } catch (err) {
        if (err.response.status === 404) {
            return 'Not Found'
        } else {
            return err.response
        }
    }
}