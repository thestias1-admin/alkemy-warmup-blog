import { useEffect, useState } from 'react';
import { ListGroup, Row, Pagination } from 'react-bootstrap';
import { getBlogList } from './utils/get_blogList'
import './BlogList.css'
import { Loading } from '../LoadingComponent/LoadingComponent'
import BlogLink from '../BlogLink/BlogLink'
import { useBlogList } from '../../contexts/bloglist-context';


function BlogList() {

    let { blogList, setBlogList } = useBlogList()
    let [loading, setLoading] = useState(true)
    let [currentPage, setCurrentPage] = useState(1)
    let blogsPerPage = 7

    function handleChangePage(event) {
        // Checks that the page clicked isnt the same as the currentPage, if not then its changed
        if (!isNaN(Number(event.target.innerText))) {
            setCurrentPage(Number(event.target.innerText))
        }
    }

    // Display logic of Blogs
    const indexOfLastBlog = currentPage * blogsPerPage;
    const indexOfFirstBlog = indexOfLastBlog - blogsPerPage;
    const currentBlogs = blogList.slice(indexOfFirstBlog, indexOfLastBlog);


    function generatePagination() {
        let pages = [];
        for (let number = 1; number <= Math.ceil(blogList.length / blogsPerPage); number++) {
            pages.push(
                <Pagination.Item key={number} active={number === currentPage} onClick={handleChangePage}>
                    {number}
                </Pagination.Item>
            );
        }
        return pages
    }

    useEffect(() => {
        // Checking if there is a blogList cached already
        let checkBlogListCache = sessionStorage.getItem('blogList')
        if (checkBlogListCache === null) {
            let blog_list = getBlogList()
            blog_list.then(data => {
                setLoading(false)
                setBlogList(data)
                sessionStorage.setItem('blogList', JSON.stringify(data))
            })
        } else { setLoading(false); setBlogList(JSON.parse(checkBlogListCache)) }
    }, [])

    return (
        <Row className="w-75 mx-auto text-light">
            {
                loading ? <Loading />
                    :
                    <>
                        <ListGroup variant="flush" className="mb-4">
                            {
                                currentBlogs.map((blog, index) => {
                                    return (
                                        <BlogLink key={blog.id} blog={blog} index={index} />
                                    )
                                })
                            }
                        </ListGroup>
                        <Pagination className="flex-wrap">{generatePagination()}</Pagination>
                    </>
            }
        </Row >
    )
}

export default BlogList