import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import LoginForm from './components/Login/Login';
import NavBar from './components/Navbar/Navbar';
import BlogList from './components/BlogList/BlogList'
import { Container } from 'react-bootstrap';
import { useAuth } from './contexts/auth-context';
import { BlogListProvider } from './contexts/bloglist-context'
import { useEffect } from 'react';
import NewBlogForm from './components/NewBlogForm/NewBlogForm';
import BlogView from './components/BlogView/BlogView'


function App() {
  // TODO: i think the redirects are missing the history handling, add it!
  // TODO: Remember to handle your scret!
  const { isLogged, setIsLogged } = useAuth()

  function setTeamToken() {
    // If there is no localStorage "team" it creates a new one
    // Runs once at page load
    let team = window.localStorage.getItem('team')
    if (team === null) {
      window.localStorage.setItem('team', JSON.stringify({
        lenght: 0,
        villans_lenght: 0,
        heroes_lenght: 0,
        characters: {}
      }))
    }
  }

  useEffect(() => {
    setTeamToken()
  }, [])

  return (
    <div className="app bg-dark min-vh-100">
      <BrowserRouter>
        <NavBar />
        <Container>
          <Switch>

            <BlogListProvider>
              <Route exact path="/">
                {isLogged ? <BlogList /> : <LoginForm />}
              </Route>

              <Route exact path="/new-blog">
                {isLogged ? <NewBlogForm /> : <LoginForm />}
              </Route>

              <Route exact path="/blog/:blogID/">
                {isLogged ? <BlogView /> : <LoginForm />}
              </Route>
            </BlogListProvider>


            <Route exact path="/login">
              {isLogged ? <Redirect to="/" /> : <LoginForm />}
            </Route>
          </Switch>
        </Container>
      </BrowserRouter>
    </div >
  );
}

export default App;